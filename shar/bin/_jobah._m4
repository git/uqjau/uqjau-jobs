#!/usr/bin/env bash

# -------------------------------------------------------------------- 
# Synopsis: A tempfile is 'seeded' with a template bash script, which
# runs '_29r_dirdefs', and set -eux. Then append your additional code
# from STDIN to this script; script is not run, but pathname echoed to
# STDOUT.  For ad hoc, one off jobs.
# -------------------------------------------------------------------- 
# Usage: $ourname < SCRIPTSOURCE
# -------------------------------------------------------------------- 
# Options:
#   -h HOSTNAME              Final script will be 'scp'd to HOSTNAME.
# -------------------------------------------------------------------- 
# Purpose: Simplify complex commandlines and meta quoting.

# ====================================================================
# Copyright (c) 2014 _Tom_Rodman_email
# --------------------------------------------------------------------
# Rating: TBD
# --------------------------------------------------------------------

# == Software License ==
# This file is part of uqjau.
#
# uqjau is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# uqjau is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with uqjau.  If not, see <http://www.gnu.org/licenses/>.
# ---

_29R_DIRDEF

#--

set -eu
ourname=${0##*/}

opt_true=1 OPTIND=1 OPT_h= OPT_v=
  # OPTIND=1 for 2nd and subsequent getopt invocations; 1 at shell start

while getopts vh: opt_char
do
   # save info in an "OPT_*" env var.
   test "$opt_char" != \? && eval OPT_${opt_char}="\"\${OPTARG:-$opt_true}\"" ||
     return 1 &>/dev/null
done
shift $(( $OPTIND -1 ))
unset opt_true opt_char

job=$(mktemp /tmp/$ourname.job.XXXXXX)

chmod +x $job

[[ -z $OPT_v ]] ||
  echo -e "$ourname: creating $job bash script from STDIN,\n  w/errexit and nounset flags enabled" >&2

cat <<\__END  > $job
#!/bin/bash -eu
eval "$(_29r_dirdefs)"
set -x
__END

cat - >> $job

cat <<\__END  >> $job

# delete self
echo rm -f $0
__END

[[ -z $OPT_v ]] ||
  echo -n "script to run: " >&2

echo $job

if [[ -n $OPT_h ]];then
  (set -x;scp $job $OPT_h:$job)
fi

exit

# #################################################################### 
# TBD explain what is below?!
# #################################################################### 

if [[ -n ${OPT_f:-} ]];then
  # list recent logs
  source $_29lib/fileutils-misc.shinc
  (find $(find /tmp -type d -name "${ourname}.*" 2>/dev/null) -type f -mtime -1 2>/dev/null)|sf -
  exit 0
fi





if [[ -n $OPT_e && $OPT_l ]];then
  echo $ourname:ERROR: -e and -l are mutually exclusive >&2
  exit 1
fi

[[ -n ${OPT_o:-} ]] && custom_log_pathname=$OPT_o || :

if [[ $# = 0 ]]; then
  set -- -
  # get job from STDIN
fi

if [[ -n ${OPT_n:-} ]];then
  script_name="$OPT_n.$ourname"
else
  script_name=""
fi

###
script=""
TMPD=$(mktemp -d /tmp/$ourname.XXXXX)
chmod 755 $TMPD

if [[ ${1:-} = - ]];then
  # job from STDIN
  shift

  script=$TMPD/${script_name:+${script_name}-}script-from-STDIN
  cat > $script
  [[ -x $script ]] || chmod +x $script

  made_script_file="yes"
  log=${custom_log_pathname:-$TMPD/${script_name:+${script_name}-}log-of-STDIN-script}
elif [[ $# = 0 ]];then
  # job from STDIN
  script=$TMPD/${script_name:+${script_name}-}script-from-STDIN
  cat > $script
  [[ -x $script ]] || chmod +x $script

  made_script_file="yes"
  log=${custom_log_pathname:-$TMPD/${script_name:+${script_name}-}log-of-STDIN-script}
else
  # job in "$@"
  # simple shell command and it's args
  script_name=${1##*/}
  made_script_file=""
  log=${custom_log_pathname:-$TMPD/log-$script_name}
fi

{
USERNAME=${USERNAME:-${USER:-$(set -e;id -n -u)}}
echo $ourname: $(date) $USERNAME@$(hostname) ${OPT_l:-in \[$PWD]} ${OPT_l:+(login shell)} to run:
echo "vvvvvvvvvvvvvvv"

for arg in "$@";do
  echo "  [$arg]"
done

if [[ -n ${script:-} ]];then
  echo
  echo script from STDIN follows:
  sed -e 's~^~  ~' $script
  echo
fi

echo STDOUT and STDERR, below three pound signs:
echo '###'
} > $log

exec <&-
# close STDIN resource

if [[ -n ${OPT_l:-} ]];then
  _shell=bash
  _shell_switches=-lic
  precmd="cd \"$PWD\";echo \( login shell w/cwd: \$PWD \);"
elif [[ -n ${OPT_e:-} ]];then
  _shell=bash
  _shell_switches=-c
  precmd="eval "'"$(_29r_dirdefs)"'";"
else
  _shell=sh
  _shell_switches=-c
  precmd=""
fi

(set -x;: logging to $log)

## main/run it
if [[ -n $made_script_file ]];then
  set -x
  setsid \
    $_shell \
    $_shell_switches \
    ${precmd:+"$precmd"}'source '"$script"'; echo "### '$ourname': retval: [$?] $(date)"' \
    >>$log  2>&1  &

    # setsid runs a program in a new session
else
  set -x
  setsid \
    $_shell \
    $_shell_switches \
    ${precmd:+"$precmd"}'"$@"              ; echo "### '$ourname': retval: [$?] $(date)"' \
    leftscript "$@" \
    >>$log  2>&1  &

    # setsid runs a program in a new session
fi
