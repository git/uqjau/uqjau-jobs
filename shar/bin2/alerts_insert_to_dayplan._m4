#!/usr/bin/env bash
set -ue

# --------------------------------------------------------------------
# Synopsis: Edit your daily plan file, adding in a report
# (typically alerts) from STDIN.
# --------------------------------------------------------------------
# Usage: $ourname
#   no switches
# --------------------------------------------------------------------
# Options: none
# Environment:
#   IMPORTANT: these env vars must be set in ~/.${ourname}rc:
#     user_checks_this_file_for_alerts_daily
#     perl_subst_cmd
#  
#       ex:
#         user_checks_this_file_for_alerts_daily=~/plan/dp3
#
#         # CAREFUL: perl one liner in 'edit_file_using_stdin-msg' is wrapped in a 'bash eval':
#         #   $msg interpolation by bash has to be deferred
#         perl_subst_cmd='"s<^(:f:fresh:.*$)><\${1}\n\$msg>"'
#
# --------------------------------------------------------------------

# ====================================================================
# RCS INFORMATION (do not touch fields bounded by $)
# --------------------------------------------------------------------
#     $Date: 2013/01/06 00:50:31 $   (GMT)
# $Revision: 1.6 $
#   $Author: rodmant $ (aka last changed by)
#   $Locker: rodmant $
#   $Source: /usr/local/7Rq/package/cur/jobs-2010.11.25/shar/bin2/RCS/alerts_insert_to_dayplan._m4,v $
#      $Log: alerts_insert_to_dayplan._m4,v $
#      Revision 1.6  2013/01/06 00:50:31  rodmant
#      *** empty log message ***
#
#
# --------------------------------------------------------------------

# ====================================================================
# Copyright (c) 2013 _Tom_Rodman_email
#
# --------------------------------------------------------------------

# == Software License ==
# This file is part of uqjau.
#
# uqjau is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# uqjau is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with uqjau.  If not, see <http://www.gnu.org/licenses/>.
# --------------------------------------------------------------------

# *****************************************************************
# define env var dir-defs for our "uqjau tools"
# *****************************************************************

_29R_DIRDEF

#--

source $_29lib/bash_common.shinc
  # for summary of what is provided run this:
  #   awk '/Overview-start/,/Overview-end/' $_lib/bash_common.shinc|less

source ~/.${ourname}rc
  # IMPORTANT: search upward for 'Environment:' for list of required entries

#--

# ####################################################################
# Main procedure.
#   script start
# ####################################################################

# main
{

  # backup before edit
  backdir=~/tmp
  mkdir -p $backdir
  VERSION_CONTROL=numbered command cp -p --backup "$user_checks_this_file_for_alerts_daily" $backdir/

  ## edit
  $_29c/edit_file_using_stdin-msg \
    -e "$perl_subst_cmd" \
    "$user_checks_this_file_for_alerts_daily"

# ==================================================
# main done.
# ==================================================
exit 0

#main end
}
