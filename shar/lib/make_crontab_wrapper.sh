#!/bin/bash
eval "$(_29r_dirdefs)"
set -x

<<\____eohd

Default 'all' target == ['combined' "*.from_combined_src"] target.
'diff' target compares "crontab -l" with "combined".  Use this before 'install',as check.

'install' target builds 'all' and then installs crontab.

Typical workflow:

    true; ./make.sh all diff
    true; ./make.sh install

____eohd


make --no-builtin-rules -f $_29lib/makefile_crontab COMBINED_SRC=$_29team/for/hostgroup/tsrUsers/crontab/combined "$@"
    : '--no-builtin-rules else make tries to build $_29lib/RCS/makefile_crontab. Not sure why.'
