#!/bin/false -meant2bsourced

# --------------------------------------------------------------------
# Synopsis: library of shell functions for script log file
#   creation and purging: mk_log_expire_named, mk_log_rotating
# Comment: w/r to log purging.. this was written when I did not
#   know about logrotate.  Is logrotate available on Cygwin?
# --------------------------------------------------------------------
#  year created: 2009
# years updated: 2010
# --------------------------------------------------------------------

# ----------------------------------------------------------------------------
# $Source: /usr/local/7Rq/package/cur/jobs-2010.11.25/shar/lib/RCS/mk_logs.shinc._m4,v $
# $Date: 2012/07/22 12:14:07 $ GMT    $Revision: 1.17 $
# ----------------------------------------------------------------------------

# ====================================================================
# Copyright (c) Oct 4 2009,2010,2011 Tom Rodman <Rodman.T.S@gmail.com>
# --------------------------------------------------------------------

# == Software License ==
# This file is part of uqjau.
#
# uqjau is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# uqjau is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with uqjau.  If not, see <http://www.gnu.org/licenses/>.
# ---

purge_logs_expired_named() {

  # -------------------------------------------------------------------- 
  # Synopsis: Delete logs listed by ls_expired_expire-named-logs.
  # -------------------------------------------------------------------- 

  # external dependencies:
  #  $_29c/ls_expired_expire-named-logs
  #  mk_log_expire_named ( for it's naming convention )

  # handy log file example:
  #  /var/local/3sBLb/log/4xlbph.1796.tigris.+@sh-script_log~%#30d#%~@+.XXakBALO

  $_29c/ls_expired_expire-named-logs "$@"|xargs --null --no-run-if-empty rm -f
  
  return 0
}

mk_log_expire_named()
{
  # -------------------------------------------------------------------- 
  # Synopsis: Logfile created w/mktemp, name suffix contains
  # number of days to live.  Expired logs are purged before new
  # one is created.
  # -------------------------------------------------------------------- 

  # options:
  #   -e EXPIRE-DAYS
  #   -n NAME
  #        defaults to $jobid.$host_short${id_or_desc:+.$id_or_desc}
  #   -i ID-OR-DESC
  #   -w LOGDIR
  #        defaults to dir named after script below $_29lg

  # external dependencies:
  #   functions: _hostname_short, purge_logs_expired_named
  #   global vars used: $jobid ( honored, but will default OK if empty of undefined ),
  #     $_29lib from $_29r/package/$_29rev/main/etc/dirdefs

  set -e
    # $FUNCNAME is intended to be used in a construct like:
    #   log=$($FUNCNAME ....)
    # this 'set -e' will not impact the parent script

  local dir
  local mktemp_STDOUT
  local suf
  local host_short
  local logtemplate
  local id_or_desc
  local expire
    # delete log after $expire (days)

    # embed a string like:
    # ~%#30d#%~

  local name
    # defining $name overides defaults, resulting in most flexible name possible:
    # $name.$suf

  while getopts :w:e:n:i: OPTCHAR
  do
     # Check for errors.
     case $OPTCHAR in
       \?) >&2 echo "Unexpected option(-$OPTARG)."
           exit 1
           # OPTCHAR is set to '?' if a nonexistent switch is specified
           # and 1st arg to getopts starts with ":"
        ;;
        :) >&2 echo "Missing parameter for option(-$OPTARG)."
           exit 1;
        ;;
        w)
          dir=$OPTARG
        ;;
        e)
          expire=$OPTARG
        ;;
        n)
          name=$OPTARG
        ;;
        i)
          id_or_desc=$OPTARG
        ;;
     esac

  done

  shift $(($OPTIND - 1))  #notice we're outside the while
  unset opt_true OPTCHAR 
  OPTIND=1

  dir=${dir:-$_29lg${ourname:+/$ourname}}
  mkdir -p "$dir"

  suf="+@sh-script_log~%#${expire:-30d}#%~@+.XXXXXXXX"
    # 'funny delimiter text' to unambigiously identify 
    #  the file as one of our log files

  if test -n "${name:-}"
  then
    logtemplate="$dir/$name.$suf"
  else
    local jobid=${jobid:-$RANDOM.$$}
      # conventional value for the global env var jobid is: TSID.$$
    host_short=$(_hostname_short)
    logtemplate="$dir/$jobid.$host_short${id_or_desc:+.$id_or_desc}.$suf"
  fi

  purge_logs_expired_named  "$dir" 

  mktemp_STDOUT=$(mktemp "$logtemplate")
  _chmod a+r "$mktemp_STDOUT"
  echo $mktemp_STDOUT
}

purge_logs_rotating() {
    # -------------------------------------------------------------------- 
    # Synopsis: Applies to logs created by mk_log_rotating; requires you
    # supply max_day_age and log dir.
    # -------------------------------------------------------------------- 

    local dir
    local max_day_age
    local max_age_min

    while getopts :w:e: OPTCHAR
    do
        # Check for errors.
        case $OPTCHAR in
            \?) >&2 echo "Unexpected option(-$OPTARG)."
                exit 1
                # OPTCHAR is set to '?' if a nonexistent switch is specified
                # and 1st arg to getopts starts with ":"
            ;;
            :) >&2 echo "Missing parameter for option(-$OPTARG)."
                exit 1;
            ;;
            w)
                dir=$OPTARG
            ;;
            e)
                max_day_age=$OPTARG
            ;;
        esac

    done

    shift $(($OPTIND - 1))  #notice we're outside the while
    unset opt_true OPTCHAR 
    OPTIND=1

    if ! ( test -n "$dir" && test -n "$max_day_age" )
    then
        errlog "$FUNCNAME:ERROR: both \$dir [$dir] and \$max_day_age [$max_day_age] must be nonempty"
        return 1 
    fi

    if ! test -d "$dir"
    then
        errlog "$FUNCNAME:ERROR: [$dir] is not a directory"
        return 2
    fi

    local max_age_min=$[${max_day_age} * 24 * 60 -(12 * 60)]
       # subtracting 12 hours to liberalize overwrite policy slightly;
       #  think about long cron jobs with logfile timestamp hours younger
       #  than start of cron job

    local find_err=$(mktemp /tmp/$FUNCNAME.find_err.XXXX)
    find "$dir" \
        -depth \
        -type f \
        -regex ".*/[0-9]+\.[^/]*\+@sh-script_log@\+\($\|\.........$\)" \
        -mmin +$max_age_min \
        -delete 2> $find_err

        # -mmin +$max_age_min -print0 2 >$find_err |xargs -r -0 rm -f

        #  -regex
        #     .*/[0-9]+\.          # log basename starts w/digits followed by a period
        #     [^/]*                # any char other than slash (so it's in basename) 
        #     \+@sh-script_log@\+  # minimal id => it's one of our log files
        #     \($                  # possible end of basename for simple names
        #        \|\.........$\)   # possible mktemp suffix, indicated by a starting literal period
        #                          #   followed by any 8 chars (the mktemp suffix)

        # xargs
        #  -r => do nothing if no STDIN
        #  -0 => understand that STDIN is using a null to separate each arg

    if [[ -s $find_err ]] && egrep -qv '^find: .*: No such file or directory$' $find_err;then
        cat $find_err >&2
    fi
    rm -f "$find_err"

    # end: purge_logs_rotating()
}

mk_log_rotating()
{
  # -------------------------------------------------------------------- 
  # Synopsis: Starts empty logfile; "rotating integer" is part of it's
  # name.  Names cycle through ( PERIODS * DAYS-IN-PERIOD ) days.
  # Expired logs are purged before new one is created.
  # -------------------------------------------------------------------- 

  # options:
  #   -p PERIODS
  #        mandatory
  #   -d DAYS-IN-PERIOD
  #        mandatory
  #   -w LOGDIR
  #        defaults to dir named after script below $_29lg
  #   -i ID-OR-DESC
  #   -s
  #        use simple name, (collision possible, no mktemp)

  # external dependencies: 
  #   functions: _hostname_short, errlog, purge_logs_rotating
  #   global vars: $jobid ( honored, but will default OK if empty of undefined ),
  #     $_29li, $_29c from $_29r/package/$_29rev/main/etc/dirdefs

  local dir
  local mktemp_STDOUT
  local suf
  local host_short
  local use_simple_name
  local periods
  local cur_period
  local days_in_period
  local max_days
  local minimal_id
  local logtemplate
  local id_or_desc

  set -e
    # $FUNCNAME is intended to be used in a construct like:
    #   log=$($FUNCNAME ....)
    # this 'set -e' will not impact the parent script

  while getopts :sw:p:d:i: OPTCHAR
  do
     # Check for errors.
     case $OPTCHAR in
       \?) >&2 echo "Unexpected option(-$OPTARG)."
           exit 1
           # OPTCHAR is set to '?' if a nonexistent switch is specified
           # and 1st arg to getopts starts with ":"
        ;;
        :) >&2 echo "Missing parameter for option(-$OPTARG)."
           exit 1;
        ;;
        s)
          use_simple_name=1
        ;;
        p)
          periods=$OPTARG
        ;;
        d)
          days_in_period=$OPTARG
        ;;
        w)
          dir=$OPTARG
        ;;
        i)
          id_or_desc=$OPTARG
        ;;
     esac

  done

  shift $(($OPTIND - 1))  #notice we're outside the while
  unset opt_true OPTCHAR 
  OPTIND=1

  if ! (test -n "$periods" && test -n "$days_in_period")
  then
    errlog -e "$FUNCNAME: \$periods [$periods] and \$days_in_period [$days_in_period]" \
      "must be not empty"
    return 1
  fi

  dir=${dir:-$_29lg${ourname:+/$ourname}}
  mkdir -p "$dir"

  minimal_id="+@sh-script_log@+"
    # 'funny delimiter text' to unambigiously identify the file as one of our log files
    # careful - this string is hardcoded in the purge function

  cur_period=$($_29c/period_in_revolution -d $days_in_period -p $periods)

  ## cleanup old logs
  max_days=$[ $days_in_period * $periods ]
  purge_logs_rotating -w "$dir" -e "$max_days"

  ## create a new empty log
  if ! test -n "${use_simple_name:-}"
  then
    # default case - use mktemp
    host_short=$(_hostname_short)
    logtemplate="$dir/$cur_period${id_or_desc:+.$id_or_desc}.$host_short.$minimal_id.XXXXXXXX"
      # 8 "X" chars in mktemp template
    mktemp_STDOUT=$(mktemp "$logtemplate")
    _chmod a+r "$mktemp_STDOUT"
    echo $mktemp_STDOUT
  else
    # simplist allowed name; collision possible, no mktemp
    touch "$dir/$cur_period${id_or_desc:+.$id_or_desc}.$minimal_id"
    echo  "$dir/$cur_period${id_or_desc:+.$id_or_desc}.$minimal_id"
  fi
}
